package org.springmvc.service;

import java.util.List;

import org.springmvc.model.User;

public interface UserService {

	User selectOne();
	
	List<User> selectList();
}
