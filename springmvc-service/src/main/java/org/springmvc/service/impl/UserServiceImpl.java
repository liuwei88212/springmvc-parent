package org.springmvc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springmvc.dao.UserMapper;
import org.springmvc.model.User;
import org.springmvc.model.UserExample;
import org.springmvc.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserMapper userMapper;

	public User selectOne() {
		User user = new User("liuwei", 30);
		return user;
	}

	public List<User> selectList() {
		return userMapper.selectByExample(new UserExample());
	}

}
