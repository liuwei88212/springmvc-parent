package org.springmvc.web.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springmvc.model.User;
import org.springmvc.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("info")
	public String info(Model model){
		
		
		
		return "user/info"; 
	}
	
	@RequestMapping("list")
	public String list(Model model){
		User user = userService.selectOne();
		
		logger.debug("{}->{}", user.getUsername(), user.getAge());
		
		model.addAttribute("info", user);
		
		
		List<User> list = userService.selectList();
		
		logger.debug("---------->"+list.size());
		
		model.addAttribute("list", list);
		
		return "user/list"; 
	}
}
